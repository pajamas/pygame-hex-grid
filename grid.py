import pygame
from math import sin, cos, pi, sqrt, floor

# help:
# https://www.redblobgames.com/grids/hexagons/

WHITE = (255,255,255)
RED = (255,0,0)
PINK = (255,200,210)
BLUE = (0,0,255)
YELLOW = (255,255,0)

class Hexagon:
    side_length = 40
    height = side_length*2
    width = 2*(side_length*cos(30*(pi/180)))
    def __init__(self, x, y, center_x_px, center_y_px):
        self.x = x
        self.y = y
        self.center_x_px = center_x_px
        self.center_y_px = center_y_px
        self.left_x = self.center_x_px - self.width/2
        self.top_y = self.center_y_px - self.height/2
        self.right_x = self.center_x_px + self.width/2
        self.bottom_y = self.center_y_px + self.height/2
        self.has_bg = False
    def to_radian(self, degrees):
        return degrees * (pi/180)
    def distance(self, point1, point2):
        return sqrt((point1[0]-point2[0])**2 + (point1[1]-point2[1])**2)
    # overly complicated hover function :D
    def hover(self):
        mouse = pygame.mouse.get_pos()
        if (mouse[0] > self.left_x and mouse[0] < self.right_x):

            # if this returns a positive number, mouse is below the top line of the hexagon
            # this x is supposed to be 0 if mouse is in the middle of the hexagon
            # and the y is supposed to be 0 at the top of this part /\ of the hexagon
            def distance_from_top(x,y):
                slope = (self.side_length/2)/(sin(self.to_radian(60))*self.side_length)
                def top_line(x):
                    return slope * self.height*0.25 - slope*abs(x)
                # flip y
                y2 = slope * self.height*0.25 - y
                return top_line(x) - y2
            
            # if this returns a positive number, mouse is above the bottom line of the hexagon
            def distance_from_bottom(x,y):
                slope = (self.side_length/2)/(sin(self.to_radian(60))*self.side_length)
                def bottom_line(x):
                    return slope*abs(x)-self.height*0.25
                # flip y
                y2 = slope * self.height*0.25 - y
                return y2 - bottom_line(x)
            
            d_top = distance_from_top(mouse[0]-self.center_x_px, mouse[1]-self.top_y)
            # idk why these numbers even work
            d_bottom = distance_from_bottom(mouse[0]-self.center_x_px, mouse[1]-self.top_y-self.side_length*1.25)

            # is mouse below top line and above bottom line
            if d_top > 1 and d_bottom > 1:
                return True
        return False

    def draw_background(self, disp, color):
            pygame.draw.rect(disp, color, (self.left_x, self.top_y + self.height*0.25,
                                            self.width, self.height*0.5))
            pygame.draw.polygon(disp, color, ((self.left_x, self.top_y + self.height*0.25),
                                               (self.right_x, self.top_y + self.height*0.25),
                                               (self.center_x_px, self.top_y)))
            pygame.draw.polygon(disp, color, ((self.left_x, self.bottom_y - self.height*0.25),
                                               (self.right_x, self.bottom_y - self.height*0.25),
                                               (self.center_x_px, self.bottom_y)))
    def draw(self, disp):
        if self.has_bg:
            self.draw_background(disp, PINK)

        left_x = self.center_x_px - self.width/2
        top_y = self.center_y_px - self.height/2
        right_x = self.center_x_px + self.width/2
        bottom_y = self.center_y_px + self.height/2
        # left side
        pygame.draw.line(disp, RED, (left_x, top_y + self.height*0.25), 
                         (left_x, top_y + self.height*0.75))
        # right side
        pygame.draw.line(disp, RED, (right_x, top_y + self.height*0.25), 
                         (right_x, top_y + self.height*0.75))
        # top left side
        pygame.draw.line(disp, RED, (left_x, top_y + self.height*0.25),
                         (self.center_x_px, top_y))
        # top right side
        pygame.draw.line(disp, RED, (right_x, top_y + self.height*0.25),
                         (self.center_x_px, top_y))
        # bottom left side
        pygame.draw.line(disp, RED, (left_x, bottom_y - self.height*0.25),
                         (self.center_x_px, bottom_y))
        # bottom right side
        pygame.draw.line(disp, RED, (right_x, bottom_y - self.height*0.25),
                         (self.center_x_px, bottom_y))

        # center
        pygame.draw.circle(disp, RED, (self.center_x_px, self.center_y_px), self.width/14)


class Grid:
    def __init__(self, X, Y, width, height):
        self.x = X
        self.y = Y
        self.width = width
        self.height = height
        self.width_px = self.width*Hexagon.width + Hexagon.width*0.5
        self.height_px = self.height*Hexagon.height*0.75 + Hexagon.height*0.25
        self.hexagons = [None] * width*height

        for x in range(width):
            for y in range(height):
                hex_x_px = X + Hexagon.width/2 + x*Hexagon.width
                if y%2 == 1:
                    hex_x_px += Hexagon.width/2
                hex_y_px = Y + Hexagon.height/2 + y*Hexagon.height*0.75
                self.hexagons[y*width + x] = Hexagon(x, y, hex_x_px, hex_y_px)
                
    def hover_hexagon_index(self):
        mouse = pygame.mouse.get_pos()
        x_in_grid = mouse[0] - self.x
        y_in_grid = mouse[1] - self.y
        # is mouse in grid?
        if x_in_grid < self.width_px and x_in_grid > 0 and y_in_grid > 0 and y_in_grid < self.height_px:
            # what row
            row_num = floor(y_in_grid / (0.75*Hexagon.height))
            # what column
            col_num = floor(x_in_grid / Hexagon.width)
            # what hexagon is mouse on if any
            for i in range(-1,1):
                for j in range(-1,1):
                    index = floor((row_num + i)*self.width + col_num + j)
                    if index < 0 or index >= len(self.hexagons):
                        continue
                    if self.hexagons[index].hover():
                        return index
        return -1

    def handle_click(self):
        hover_index = self.hover_hexagon_index()
        if hover_index != -1:
            self.hexagons[hover_index].has_bg = not self.hexagons[hover_index].has_bg

    def draw(self, disp):
        def draw_hexagons():
            for y in range(self.height):
                for x in range(self.width):
                    self.hexagons[y*self.width + x].draw(disp)
        # draw blue border
        pygame.draw.rect(disp, BLUE, 
                        (self.x-1, self.y-1, self.width_px + 3, 
                         self.height_px + 3), 1)
        mouse = pygame.mouse.get_pos()
        x_in_grid = mouse[0] - self.x
        y_in_grid = mouse[1] - self.y
        
        hover_index = self.hover_hexagon_index()
        if hover_index != -1:
            self.hexagons[hover_index].draw_background(disp, YELLOW)
        draw_hexagons()
        return

        # is mouse in grid?
        if x_in_grid < self.width_px and x_in_grid > 0 and y_in_grid > 0 and y_in_grid < self.height_px:
            # what row
            row_num = floor(y_in_grid / (0.75*Hexagon.height))
            # what column
            col_num = floor(x_in_grid / Hexagon.width)
            # what hexagon is mouse on if any
            for i in range(-1,1):
                for j in range(-1,1):
                    index = floor((row_num + i)*self.width + col_num + j)
                    if index < 0 or index >= len(self.hexagons):
                        continue
                    if self.hexagons[index].hover():
                        self.hexagons[index].draw_background(disp, YELLOW)
                        # draw hexagons
                        draw_hexagons()
                        return
        draw_hexagons()
            


grid = Grid(X=10,Y=10,width=11,height=12)

pygame.init()
DISP = pygame.display.set_mode((grid.width_px+grid.x*2,grid.height_px+grid.y*2))
FPS = pygame.time.Clock()



running = True
while running:
    FPS.tick(60)
    for event in pygame.event.get():    
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            grid.handle_click()
    
    DISP.fill(WHITE)
    grid.draw(DISP)

    pygame.display.update()
